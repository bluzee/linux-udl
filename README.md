# Linux-udl #

**Meltdown security patch PTI is not enabled by default**

If you wish to use PTI  edit the .config
file and set *CONFIG_PAGE_TABLE_ISOLATION=y*
then run makepkg.

PKGBUILD to create Arch Linux packages from v4l-updatelee.

https://bitbucket.org/updatelee/v4l-updatelee

This provides the patched kernel modules and DVB api 
headers required by the updateDVB spectrum scanning
program. 

https://bitbucket.org/updatelee/updatedvb

**Warning**

If you use nvidia dkms module drivers they will probably
not compile with a 4.16 kernel without patching the nvidia
source and re-installing. You may wish to change to the 
**nouveau** driver. Other dkms drivers may fail to compile 
as well. You'll need to check these. This is a development 
version kernel. 

### How do I get set up? ###

git clone https://bitbucket.org/bluzee/linux-udl

cd linux-udl

makepkg -s

Installing the packages can be done with either makepkg -i 
or pacman -U. Docs package should be optional. You'll need the 
headers, api-headers and finally the linux-udl kernel package. 

Now update your bootloader config.

https://wiki.archlinux.org/index.php/Category:Boot_loaders

https://wiki.archlinux.org/index.php/GRUB#Generate_the_main_configuration_file

Reboot choosing the new kernel from the grub menu.

### Firmware ###

If you have not previously installed any needed firmware for
your device you may find the required firmware file in...

src/firmware

Alternatively you can install all firmware file by doing....

cd src/firmware

sudo make install

